package storage

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"time"
)

type Storage interface {
	CreateTask(task *Task) error
	GetAllTasks() ([]*Task, error)
	GetTasksByAuthorID(authorID int64) ([]*Task, error)
	GetTasksByLabelID(labelID int64) ([]*Task, error)
	UpdateTaskByID(id int64, task *Task) error
	DeleteTaskByID(id int64) error
}

func New(url string) (Storage, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	pool, err := pgxpool.Connect(ctx, url)
	if err != nil {
		return nil, err
	}
	return &storage{db: pool}, nil
}

type storage struct {
	db *pgxpool.Pool
}

func (s *storage) CreateTask(task *Task) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	query := `INSERT INTO 
"tasks"("opened", "closed", "author_id", "assigned_id", "title", "content") 
VALUES($1, $2, $3, $4, $5, $6)`
	_, err := s.db.Exec(ctx, query,
		task.Opened,
		task.Closed,
		task.AuthorID,
		task.AssignedID,
		task.Title,
		task.Content)
	return err
}

func (s *storage) scanTask(rows pgx.Rows) (*Task, error) {
	task := new(Task)
	err := rows.Scan(
		&task.ID,
		&task.Opened,
		&task.Closed,
		&task.AuthorID,
		&task.AssignedID,
		&task.Title,
		&task.Content)
	return task, err
}

func (s *storage) scanTasks(rows pgx.Rows) ([]*Task, error) {
	tasks := make([]*Task, 0)
	for rows.Next() {
		task, err := s.scanTask(rows)
		if err != nil {
			return nil, err
		}
		tasks = append(tasks, task)
	}
	return tasks, nil
}

func (s *storage) GetAllTasks() ([]*Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	rows, err := s.db.Query(ctx, `SELECT * FROM "tasks"`)
	if err != nil {
		return nil ,err
	}
	return s.scanTasks(rows)
}

func (s *storage) GetTasksByAuthorID(authorID int64) ([]*Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	rows, err := s.db.Query(ctx, `SELECT * FROM "tasks" WHERE "author_id"=$1`, authorID)
	if err != nil {
		return nil ,err
	}

	return s.scanTasks(rows)
}

func (s *storage) GetTasksByLabelID(labelID int64) ([]*Task, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	query :=
`SELECT 
"id", 
"opened", 
"closed", 
"author_id", 
"assigned_id", 
"title", 
"content" 
FROM "tasks"
JOIN "labels" ON "task_id" = "id"
WHERE "label_id"=$1`
	rows, err := s.db.Query(ctx, query, labelID)
	if err != nil {
		return nil ,err
	}
	return s.scanTasks(rows)
}

func (s *storage) UpdateTaskByID(id int64, task *Task) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	query :=
`UPDATE "tasks" SET
"opened"=$1, 
"closed"=$2,
"author_id"=$3,
"assigned_id"=$4, 
"title"=$5, 
"content"=$6
WHERE "id"=$7
`
	_, err := s.db.Exec(ctx, query,
		task.Opened,
		task.Closed,
		task.AuthorID,
		task.AssignedID,
		task.Title,
		task.Content,
		id)
	return err
}

func (s *storage) DeleteTaskByID(id int64) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 3)
	defer cancel()
	query :=
`DELETE FROM "tasks"
WHERE "id"=$1
`
	_, err := s.db.Exec(ctx, query, id)
	return err
}