package storage

type User struct {
	ID   int64
	Name string
}

type Task struct {
	ID         int64
	Opened     int64
	Closed     int64
	AuthorID   int64
	AssignedID int64
	Title      string
	Content    string
}

type Labels struct {
	ID   int64
	Name string
}

type TasksLabels struct {
	TaskID  int64
	LabelID int64
}
