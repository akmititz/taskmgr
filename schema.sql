CREATE TABLE IF NOT EXISTS "users" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS "tasks" (
    "id" BIGSERIAL PRIMARY KEY,
    "opened" BIGINT,
    "closed" BIGINT,
    "author_id" BIGINT NOT NULL,
    "assigned_id" BIGINT NOT NULL,
    "title" TEXT,
    "content" TEXT
);

CREATE TABLE IF NOT EXISTS "labels" (
    "id" BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS "tasks_labels" (
    "task_id" BIGINT NOT NULL,
    "label_id" BIGINT NOT NULL
);

ALTER TABLE IF EXISTS "tasks" ADD FOREIGN KEY ("author_id") REFERENCES "users"("id");
ALTER TABLE IF EXISTS "tasks" ADD FOREIGN KEY ("assigned_id") REFERENCES "users"("id");
ALTER TABLE IF EXISTS "tasks_labels" ADD FOREIGN KEY ("task_id") REFERENCES "tasks"("id");
ALTER TABLE IF EXISTS "tasks_labels" ADD FOREIGN KEY ("label_id") REFERENCES "labels"("id");